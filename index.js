require('./models/mongodb')
const productController = require('./controllers/productController');
const categoryController = require('./controllers/categoryController');
const userController = require('./controllers/UserController');
const auth = require('./controllers/auth')
const authMiddleware = require('./middlewares/auth')
const config = require('config')
    //Import the necessary packages
const express = require('express');

var app = express();
const bodyparser = require('body-parser');

app.use(bodyparser.urlencoded({
    extended: true
}));
app.use(bodyparser.json());
app.get('/', (req, res) => {
    res.send('Welcome to our shopping management system');
});
console.log(config.get("jwtPrivateKey"));
//Set the Controller path which will be responding the user actions
app.use('/api/products', authMiddleware, productController);
app.use('/api/categories', authMiddleware, categoryController);
app.use('/api/users', userController)
app.use('/api/auth', auth)
    //PORT ENVIRONMENT VARIABLE
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}..`));