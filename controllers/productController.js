const express = require('express');
const mongoose = require('mongoose');
var router = express.Router();
const Product = mongoose.model('Products');
const Category = mongoose.model("Categories")
const admin = require('../middlewares/isAdmin')
    //get all products
router.get('/', (req, res) => {
    Product.find()
        .then(products => res.send(products))
        .catch(err => res.send(err).status(404));
});

//insert new record
router.post('/', admin, (req, res) => {
    //find if category exist
    let category = Category.findById({
        _id: req.body.categoryId
    })
    if (!category) {
        return res.send("category with that id not found").status(400)
    } else {
        let product = new Product();
        product.name = req.body.name;
        product.price = req.body.price;
        product.categoryId = req.body.categoryId;
        product.save()
            .then(productSaved => res.send(productSaved).status(201))
            .catch(err => res.send(err).status(400));
    }

});

//update
router.put('/:id', admin, (req, res) => {
    Product.findOneAndUpdate({
                _id: req.params.id
            },
            req.body, {
                new: true
            })
        .then(product => res.send(product))
        .catch(err => res.send(err).status(400));
});

//delete 
router.delete('/:id', admin, (req, res) => {
        Product.findByIdAndDelete({
                _id: req.params.id
            })
            .then(product => res.status(200).send(product))
            .catch(err => res.status(400).send(err))
    })
    //Get one by id
router.get('/byId/:id', (req, res) => {
        Product.findById({
                _id: req.params.id
            })
            .then(product => res.status(200).send(product))
            .catch(err => res.status(400).send(err))
    })
    //get all by category id
router.get('/byCategory/:categoryId', (req, res) => {
    Product.find({
            categoryId: req.params.categoryId
        })
        .then(products => res.status(200).send(products))
        .catch(err => res.status(400).send(err))
})
module.exports = router;