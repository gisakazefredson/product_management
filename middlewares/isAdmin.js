const jwt = require("jsonwebtoken")
const config = require("config")

function isAdmin(req, res, next) {
    const token = req.header('x-auth-token')
    const decod = jwt.verify(token, config.get('jwtPrivateKey'))
    req.user = decod
    if (req.user.isAdmin == "false") {
        return res.send("action not permited!");
    } else {
        next();
    }
}
module.exports = isAdmin